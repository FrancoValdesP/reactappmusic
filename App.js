import React from 'react'
import { StyleSheet, ScrollView} from 'react-native'
import ArtistBox from './Components/ArtistBox'

export default class App extends React.Component {
  render() {
    const artist = {
        image: 'https://lh3.googleusercontent.com/SO2hJu3L6dEC4mDUo8OrvLy3eP6n0y6RB7vmmHedHG-hlXfbFeIgHVie0bpTxPraWg=h900',
        name: 'Doge - F.I.F.O',
        likes: 100,
        comments: 168
    }
    return (
      <ScrollView style={styles.container}>
         {Array(15).fill(artist).map(artist => {
             return <ArtistBox artist={artist}></ArtistBox>
         })
        }
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop : 50
}
})